﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SabberStoneCoreAi.src.Agent
{
	// Basically an array of weights
	class NNGenotype
    {
		float[] data;
		const int LEN = 351;

		// creates random genome
		public NNGenotype()
		{
			data = new float[LEN];
			Random r = new Random();
			for (int i = 0; i < LEN; i++)
			{
				data[i] = (float) r.NextDouble();
			}
		}

		// reads genome from file at path
		public NNGenotype(string path)
		{
			string raw = File.ReadAllText(path);
			data = Parse(raw);
		}

		public NNGenotype(float[] d)
		{
			data = d;
		}

		public float[] Data { get => data; }

		public void SaveTo(string path)
		{
			File.WriteAllText(path, Encode());
		}

		// string -> data
		public static float[] Parse(string raw)
		{
			float[] parsed = new float[LEN];
			string[] raw_split = raw.Split(" ");

			for (int i = 0; i < raw_split.Length && i < parsed.Length; i++)
			{
				parsed[i] = Single.Parse(raw_split[i]);
			}

			return parsed;
		}

		private string Encode()
		{
			string str = "";

			foreach (float f in data)
			{
				str += f.ToString("N7");
				str += " ";
			}

			return str;
		}

		//EA Stuff

		// Maybe (as in p=1) mutates genome, changes are in place
		public void Mutate(Random rng)
		{
			int idx = rng.Next(LEN);
			data[idx] += ((float) rng.NextDouble() - 0.5f);
		}

		public static NNGenotype Crossover(NNGenotype g1, NNGenotype g2, Random rng)
		{
			float[] tmp = new float[LEN];
			int idx = rng.Next(LEN);

			for (int i = 0; i < idx; i++)
			{
				tmp[i] = g1.Data[i];
			}

			for (int i = idx; i < LEN; i++)
			{
				tmp[i] = g2.Data[i];
			}

			return new NNGenotype(tmp);
		}
	}
}
