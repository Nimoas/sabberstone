﻿using SabberStoneCore.Tasks;
using SabberStoneCoreAi.Agent;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace SabberStoneCoreAi.src.Agent
{
	class DumbAgent : AbstractAgent
	{
		private Random Rng = new Random(42);

		public override void FinalizeAgent()
		{
			//throw new NotImplementedException();
		}

		public override void FinalizeGame()
		{
			//throw new NotImplementedException();
		}

		// Called multiple times by simulation
		// Turn end with TurnEndTask
		public override PlayerTask GetMove(POGame.POGame poGame)
		{
			List<PlayerTask> options = poGame.CurrentPlayer.Options();
			foreach (PlayerTask t in options)
			{
				Console.WriteLine("\n\nPlayerTask:");
				foreach (PropertyDescriptor desc in TypeDescriptor.GetProperties(t))
				{
					string name = desc.Name;
					object val = desc.GetValue(t);
					Console.WriteLine("{0} = {1}", name, val);
				}
			}

			foreach (PropertyDescriptor desc in TypeDescriptor.GetProperties(poGame))
			{
				string name = desc.Name;
				object val = desc.GetValue(poGame);
				Console.WriteLine("{0} = {1}", name, val);
			}

			return options[Rng.Next(options.Count)];
		}

		public override void InitializeAgent()
		{
			//throw new NotImplementedException();
		}

		public override void InitializeGame()
		{
			//throw new NotImplementedException();
		}
	}
}
