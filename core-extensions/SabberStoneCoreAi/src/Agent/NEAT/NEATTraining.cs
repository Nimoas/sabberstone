﻿using SabberStoneCore.Config;
using SabberStoneCore.Enums;
using SabberStoneCore.Model;
using SabberStoneCoreAi.Agent;
using SabberStoneCoreAi.Agent.ExampleAgents;
using SabberStoneCoreAi.Meta;
using SabberStoneCoreAi.POGame;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SabberStoneCoreAi.src.Agent.NEAT
{
    class NEATTraining
    {
		const int MAX_EPOCHS = 1000;
		const int POP_SIZE = 75;
		const int EVAL_RUNS = 250;
		const int NUM_OFFSPRING = 100;
		const int TOURNAMENT_SIZE = 4;
		const string BASE_PATH = "C:\\Training\\NEAT_RLE_Mirror_Pirate_2\\";

		const float WEIGHT_MUTATION_RATE = 0.01f;
		const float CONNECTION_MUTATION_RATE = 0.05f;
		const float NODE_MUTATION_RATE = 0.01f;

		const float DISTANCE_C1 = 2.0f;
		const float DISTANCE_C2 = 1.0f;
		const float DISTANCE_CHANGE = 0.15f;
		float distance_thres = 0.5f;
		const int TARGET_NUM_SPECIES = 20;

		Random rng = new Random();


		public void RunTraining()
		{
			/*################
			INITIALIZATION
			 #################*/

			int epoch = 0;
			List<NEATGenome> pop = new List<NEATGenome>(POP_SIZE + NUM_OFFSPRING);
			int numNodes = NEATAgent.NUM_INPUTS + 1;
			int nextConnectionId = NEATAgent.NUM_INPUTS + 1;

			if (Directory.Exists(BASE_PATH))
			{
				string[] subdirs = Directory.GetDirectories(BASE_PATH);
				if (subdirs.Length > 0)
				{
					int max = -1;
					foreach (string s in subdirs)
					{
						int e = Int32.Parse(Path.GetFileName(s));
						max = Math.Max(max, e);
					}
					if (max >= 0)
					{
						epoch = max + 1;
						string path = BASE_PATH + max + "\\";

						int maxNode = numNodes-1;
						int maxId = nextConnectionId-1;

						for (int i = 0; i < POP_SIZE; i++)
						{
							NEATGenome gene = NEATGenome.LoadFrom(path + i);
							Evaluate(gene);
							pop.Add(gene);
							foreach (ConnectionGene c in gene.Connections)
							{
								maxId = Math.Max(maxId, c.Id);
								maxNode = Math.Max(Math.Max(maxNode, c.InNode), c.OutNode);
							}
						}

						numNodes = maxNode + 1;
						nextConnectionId = maxId + 1;
					}
					else
					{
						// Random init of population
						for (int i = 0; i < POP_SIZE; i++)
						{
							NEATGenome gene = new NEATGenome(NEATAgent.NUM_INPUTS);
							Evaluate(gene);
							pop.Add(gene);
						}
					}
				}
				else
				{
					// Random init of population
					for (int i = 0; i < POP_SIZE; i++)
					{
						NEATGenome gene = new NEATGenome(NEATAgent.NUM_INPUTS);
						Evaluate(gene);
						pop.Add(gene);
					}
				}
			}
			else
			{
				// Random init of population
				for (int i = 0; i < POP_SIZE; i++)
				{
					NEATGenome gene = new NEATGenome(NEATAgent.NUM_INPUTS);
					Evaluate(gene);
					pop.Add(gene);
				}
			}

			/*#########################
			MAIN ALGORITHM - EPOCH LOOP
			 ##########################*/

			float bestFitness = 0;
			Speciate(pop);
			

			while (epoch < MAX_EPOCHS)
			{
				Console.WriteLine("Epoch {0}", epoch);

				/*######################
				MATING
				 ######################*/
				List<NEATGenome> offspring = new List<NEATGenome>(NUM_OFFSPRING);
				List<ConnectionGene> conInnovations = new List<ConnectionGene>();
				List<(ConnectionGene, int, int, int)> nodeInnovations = new List<(ConnectionGene, int, int, int)>(); // split connections
				for (int i = 0; i < NUM_OFFSPRING; i++)
				{
					// MATING SELECTION
					NEATGenome parent1 = TournamentSelection(pop);
					NEATGenome parent2 = TournamentSelection(pop);
					NEATGenome fitterParent = (parent1.AdjustedFitness > parent2.AdjustedFitness) ? parent1 : parent2;
					NEATGenome noobParent = (parent1.AdjustedFitness <= parent2.AdjustedFitness) ? parent1 : parent2;

					/*#######################
					 * CROSSOVER
					 ########################*/
					List<ConnectionGene> cons = new List<ConnectionGene>(20);
					// Conncetions are sorted by id in Genome constructor !!!
					int fitterIndex = 0;
					int noobIndex = 0;
					while (fitterIndex < fitterParent.Connections.Length && noobIndex < noobParent.Connections.Length)
					{
						int fitterId = fitterParent.Connections[fitterIndex].Id;
						int noobId = noobParent.Connections[noobIndex].Id;
						// fitter has Connection not existing in other
						if (fitterId < noobId)
						{
							cons.Add(fitterParent.Connections[fitterIndex]);
							fitterIndex++;
						}
						// noob has connection not in other - ignore
						else if (noobId > fitterId)
						{
							noobIndex++;
						}
						// equality case
						else
						{
							if (rng.NextDouble() > 0.5)
							{
								cons.Add(fitterParent.Connections[fitterIndex]);
							}
							else
							{
								cons.Add(noobParent.Connections[noobIndex]);
							}
							fitterIndex++;
							noobIndex++;
						}
					}

					// copy over rest of fitter parent
					while (fitterIndex < fitterParent.Connections.Length)
					{
						cons.Add(fitterParent.Connections[fitterIndex]);
						fitterIndex++;
					}

					/*#########################
					 MUTATION
					 ##########################*/

					// Add connection
					if (rng.NextDouble() < CONNECTION_MUTATION_RATE)
					{
						int in_ = rng.Next(numNodes);
						int out_ = rng.Next(numNodes - NEATAgent.NUM_INPUTS) + NEATAgent.NUM_INPUTS;
						// ignore self connections
						// TODO maybe ignore already existing ones
						bool exists = false;

						foreach(ConnectionGene c in cons)
						{
							if (c.InNode == in_ && c.OutNode == out_)
							{
								exists = true;
							}
						}

						if (in_ != out_ && !exists)
						{
							int idx = -1;
							for (int j = 0; j < conInnovations.Count; j++)
							{
								if (conInnovations[j].InNode == in_ && conInnovations[j].OutNode == out_)
								{
									idx = j;
								}
							}

							if (-1 == idx)
							{
								ConnectionGene newCon = new ConnectionGene(nextConnectionId++, in_, out_, (float) rng.NextDouble() -0.5f);
								cons.Add(newCon);
								conInnovations.Add(newCon);
							//	Console.WriteLine("\t\tAdded con from {0} to {1}", in_, out_);
							} else
							{
								ConnectionGene newCon = new ConnectionGene(conInnovations[idx].Id, in_, out_, (float)rng.NextDouble() - 0.5f);
								cons.Add(newCon);
							}
						}
					}

					// Add node
					if (NODE_MUTATION_RATE > rng.NextDouble())
					{
						int idxToSplit = rng.Next(cons.Count);

						//Console.WriteLine("\t\tAdding node from {0} to {1}", cons[idxToSplit].InNode, cons[idxToSplit].OutNode);

						int idx = -1;
						for (int j = 0; j < nodeInnovations.Count; j++)
						{
							if (nodeInnovations[j].Item1.Id == cons[idxToSplit].Id)
							{
								idx = j;
							}
						}

						if (idx == -1)
						{
							// New innovation
							int nodeId = numNodes++;
							ConnectionGene newCon1 = new ConnectionGene(
								nextConnectionId++,
								cons[idxToSplit].InNode,
								nodeId,
								(float) rng.NextDouble() - 0.5f);
							ConnectionGene newCon2 = new ConnectionGene(
								nextConnectionId++,
								nodeId,
								cons[idxToSplit].OutNode,
								(float)rng.NextDouble() - 0.5f);

							nodeInnovations.Add((cons[idxToSplit], newCon1.Id, newCon2.Id, nodeId));
							cons.RemoveAt(idxToSplit);
							cons.Add(newCon1);
							cons.Add(newCon2);
						}
						else
						{
							// already existing innovation
							int nodeId = nodeInnovations[idx].Item4;
							ConnectionGene newCon1 = new ConnectionGene(
								nodeInnovations[idx].Item2,
								cons[idxToSplit].InNode,
								nodeId,
								(float)rng.NextDouble() - 0.5f);
							ConnectionGene newCon2 = new ConnectionGene(
								nodeInnovations[idx].Item3,
								nodeId,
								cons[idxToSplit].OutNode,
								(float)rng.NextDouble() - 0.5f);

							cons.RemoveAt(idxToSplit);
							cons.Add(newCon1);
							cons.Add(newCon2);
						}
					}

					// Mutate weights
					for (int j = 0; j < cons.Count; j++)
					{
						if (WEIGHT_MUTATION_RATE > rng.NextDouble())
						{
							float weightFactor = 0.25f* ((float) rng.NextDouble() - 0.5f);
							ConnectionGene newCon = new ConnectionGene(cons[j].Id, cons[j].InNode, cons[j].OutNode, cons[j].Weight + weightFactor);
							cons[j] = newCon;
							//Console.WriteLine("\t\tMutated weight");
						}
					}

					// ADDING TO POP
					NEATGenome off = new NEATGenome(cons.ToArray());
					Evaluate(off);
					offspring.Add(off);
					if (i % 25 == 0 && i > 1)
					{
						Console.WriteLine("\t\tCreated {0} Offspring.", i+1);
					}
				}
				pop.AddRange(offspring);

				Console.WriteLine("\tFinished Mating.");

				// SPECIATE again
				Speciate(pop);

				NEATGenome best = pop[0];
				bestFitness = 0;

				// Find best and keep it
				foreach (NEATGenome ph in pop)
				{
					if (ph.Fitness > bestFitness)
					{
						best = ph;
						bestFitness = ph.Fitness;
					}
				}

				// Print statistics
				Console.WriteLine("\tBest fitness is {0}", best.Fitness);
				Console.WriteLine("\tBest adjusted fitness is {0}", best.AdjustedFitness);
				float avrg = 0;
				float avrgAdjusted = 0;
				foreach (NEATGenome ph in pop)
				{
					avrg += ph.Fitness;
					avrgAdjusted += ph.AdjustedFitness;
				}
				avrg /= (float)pop.Count;
				avrgAdjusted /= (float)pop.Count;
				Console.WriteLine("\tAvrg. fitness is {0}", avrg);
				Console.WriteLine("\tAvrg. adjusted fitness is {0}", avrgAdjusted);

				// new population
				List<NEATGenome> nextGen = new List<NEATGenome>(POP_SIZE + NUM_OFFSPRING);
				// take best
				nextGen.Add(best);

				for (int i = 1; i < POP_SIZE; i++)
				{
					nextGen.Add(TournamentSelection(pop));
				}
				pop = nextGen;

				// Save stuff to disk... VM crashed quite regularly
				string path = BASE_PATH + epoch + "\\";
				Directory.CreateDirectory(path);
				best.SaveTo(path + "best");
				for (int i = 0; i < POP_SIZE; i++)
				{
					string filename = path + i;
					pop[i].SaveTo(filename);
				}

				epoch++;
			}


		}

		/**
		 * Recalculates adjusted fitness values for each individual
		 **/
		private void Speciate(List<NEATGenome> pop)
		{
			Console.WriteLine("\tStarting Speciesation");
			List < List < NEATGenome >> species = new List<List<NEATGenome>>(TARGET_NUM_SPECIES);

			foreach(NEATGenome g in pop)
			{
				int idx = -1;
				for (int i = 0; i < species.Count; i++)
				{
					//Console.WriteLine("\t\t\t\t{0}", NEATGenome.Similarity(g, species[i][0], DISTANCE_C1, DISTANCE_C2));
					if (NEATGenome.Similarity(g, species[i][0], DISTANCE_C1, DISTANCE_C2) < distance_thres)
					{
						idx = i;
						break;
					}
				}
				if (idx == -1)
				{
					// new species
					species.Add(new List<NEATGenome>());
					species[species.Count - 1].Add(g);
					//Console.WriteLine("\t\t\tSorted into new species");
				}
				else
				{
					// add to existing
					species[idx].Add(g);
					//Console.WriteLine("\t\t\tSorted into existing species");
				}
			}

			Console.WriteLine("\t\tFinished splitting species - {0}", species.Count);

			foreach (List<NEATGenome> spec in species)
			{
				foreach (NEATGenome g in spec)
				{
					float sum = 0;
					foreach (NEATGenome g_ in spec)
					{
						sum += NEATGenome.Similarity(g, g_, DISTANCE_C1, DISTANCE_C2);
					}
					if (sum <= 1)
					{
						sum = 1;
					}
					g.AdjustedFitness = g.Fitness / sum;
				}
			}

			Console.WriteLine("\t\tFinished adjusting fitness");

			int numSpec = species.Count;
			if (numSpec > TARGET_NUM_SPECIES + 1)
			{
				distance_thres += DISTANCE_CHANGE;
			} else if (numSpec < TARGET_NUM_SPECIES - 1)
			{
				distance_thres -= DISTANCE_CHANGE;
			}

			Console.WriteLine("\t\tnew distance threshold {0}", distance_thres);
		}

		// Uses adjusted fitness
		private NEATGenome TournamentSelection(List<NEATGenome> pop)
		{
			NEATGenome best = pop[0];
			float fitness = 0;

			for (int i = 0; i < TOURNAMENT_SIZE; i++)
			{
				int idx = rng.Next(pop.Count);
				NEATGenome ph = pop[idx];
				if (ph.AdjustedFitness >= fitness)
				{
					best = ph;
					fitness = ph.AdjustedFitness;
				}
			}

			return best;
		}

		private void Evaluate(NEATGenome ph)
		{
			float avrg = 0;

			avrg += SubEvaluate(ph, Decks.AggroPirateWarrior, Decks.AggroPirateWarrior, CardClass.WARRIOR, CardClass.WARRIOR);
			//avrg += SubEvaluate(ph, Decks.AggroPirateWarrior, Decks.RenoKazakusMage, CardClass.WARRIOR, CardClass.MAGE);
			//avrg += SubEvaluate(ph, Decks.AggroPirateWarrior, Decks.MidrangeJadeShaman, CardClass.WARRIOR, CardClass.SHAMAN);

			/*avrg += SubEvaluate(ph, Decks.RenoKazakusMage, Decks.AggroPirateWarrior, CardClass.MAGE, CardClass.WARRIOR);
			avrg += SubEvaluate(ph, Decks.RenoKazakusMage, Decks.RenoKazakusMage, CardClass.MAGE, CardClass.MAGE);
			avrg += SubEvaluate(ph, Decks.RenoKazakusMage, Decks.MidrangeJadeShaman, CardClass.MAGE, CardClass.SHAMAN);
			
			avrg += SubEvaluate(ph, Decks.MidrangeJadeShaman, Decks.AggroPirateWarrior, CardClass.SHAMAN, CardClass.WARRIOR);
			avrg += SubEvaluate(ph, Decks.MidrangeJadeShaman, Decks.RenoKazakusMage, CardClass.SHAMAN, CardClass.MAGE);
			avrg += SubEvaluate(ph, Decks.MidrangeJadeShaman, Decks.MidrangeJadeShaman, CardClass.SHAMAN, CardClass.SHAMAN);
			
			avrg /= 6.0f;*/
			ph.Fitness = avrg;
		}

		private float SubEvaluate(NEATGenome ph, List<Card> d1, List<Card> d2, CardClass c1, CardClass c2)
		{
			GameConfig gameConfig = new GameConfig
			{
				Player1Deck = d1,
				Player2Deck = d2,
				Player1HeroClass = c1,
				Player2HeroClass = c2,
				Logging = false
			};

			AbstractAgent player1 = new NEATAgent(ph);
			AbstractAgent player2 = new RandomAgentLateEnd();
			var gameHandler = new POGameHandler(gameConfig, player1, player2, debug: false);

			//gameHandler.PlayGame();
			gameHandler.PlayGames(EVAL_RUNS);
			GameStats gameStats = gameHandler.getGameStats();

			//gameStats.printResults();
			return (float)gameStats.PlayerA_Wins / (float)gameStats.GamesPlayed * 100f;

		}
	}
}
