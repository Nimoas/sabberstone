﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SabberStoneCoreAi.src.Agent.NEAT
{
    class ConnectionGene: IComparable
    {
		private readonly int id;
		private readonly int inNode;
		private readonly int outNode;
		private readonly float weight;

		public ConnectionGene(int id, int in_, int out_, float w)
		{
			this.id = id;
			inNode = in_;
			outNode = out_;
			weight = w;
		}

		public int Id => id;

		public int InNode => inNode;

		public int OutNode => outNode;

		public float Weight => weight;

		public int CompareTo(object obj)
		{
			ConnectionGene c = (ConnectionGene) obj;
			return Id - c.Id;
		}
	}
}
