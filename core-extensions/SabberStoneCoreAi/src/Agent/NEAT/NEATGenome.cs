﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SabberStoneCoreAi.src.Agent.NEAT
{
	/*
	BASIC ASSUMPTION: The first n nodes are inputs, the next is output, rest is hidden
		 */
    class NEATGenome
    {
		private ConnectionGene[] connections;
		private float fitness = 0;
		private float adjustedFitness = 0;

		public NEATGenome (ConnectionGene[] cons)
		{
			connections = cons;
			Array.Sort(connections);
		}

		public NEATGenome (int numInputs)
		{
			Random rng = new Random();
			int outputId = numInputs;
			connections = new ConnectionGene[numInputs];
			for (int i = 0; i < numInputs; i++)
			{
				connections[i] = new ConnectionGene(i, i, outputId, (float) rng.NextDouble() - 0.5f);
			}
		}

		public float Fitness { get => fitness; set => fitness = value; }
		public float AdjustedFitness { get => adjustedFitness; set => adjustedFitness = value; }

		public ConnectionGene[] Connections => connections;

		public static NEATGenome LoadFrom(string path)
		{
			string raw = File.ReadAllText(path);
			return new NEATGenome(Parse(raw));
		}

		public static ConnectionGene[] Parse(string raw)
		{
			string[] split = raw.Split("#");
			ConnectionGene[] cons = new ConnectionGene[split.Length-1];
			for (int i = 0; i < split.Length-1; i++)
			{
				string[] line_split = split[i].Split(" ");
				cons[i] = new ConnectionGene(
					Int32.Parse(line_split[0]),
					Int32.Parse(line_split[1]),
					Int32.Parse(line_split[2]),
					Single.Parse(line_split[3])
					);
			}
			return cons;
		}

		public void SaveTo(string path)
		{
			File.WriteAllText(path, Encode());
		}

		private string Encode()
		{
			string str = "";
			foreach (ConnectionGene c in connections)
			{
				str += c.Id;
				str += " ";
				str += c.InNode;
				str += " ";
				str += c.OutNode;
				str += " ";
				str += c.Weight.ToString("E10");
				str += "#";
			}
			return str;
		}

		// just picking same coefficient for disjoint and excess
		public static float Similarity(NEATGenome g1, NEATGenome g2, float c1, float c2)
		{
			//Console.WriteLine("\t\t\t\tCalled Similarity");
			int N = Math.Max(g1.Connections.Length, g2.Connections.Length);

			int idx1 = 0;
			int idx2 = 0;

			int sumDisjoint = 0;
			float sumWeightDif = 0;

			while (idx1 < g1.Connections.Length && idx2 < g2.Connections.Length)
			{
				int id1 = g1.Connections[idx1].Id;
				int id2 = g2.Connections[idx2].Id;
				if (id1 < id2)
				{
					sumDisjoint++;
					idx1++;
				} else if (id1 > id2)
				{
					sumDisjoint++;
					idx2++;
				} else
				{
					sumWeightDif += Math.Abs(g1.Connections[idx1].Weight - g2.Connections[idx2].Weight);
					idx1++;
					idx2++;
				}
			}

			while (idx1 < g1.Connections.Length)
			{
				sumDisjoint++;
				idx1++;
			}

			while (idx2 < g2.Connections.Length)
			{
				sumDisjoint++;
				idx2++;
			}

			return (c1 * (float) sumDisjoint / (float) N) + (c2 * sumWeightDif / (float) (N - sumDisjoint));
		}
	}
}
