﻿using SabberStoneCore.Model.Entities;
using SabberStoneCore.Tasks;
using SabberStoneCoreAi.Agent;
using SabberStoneCoreAi.POGame;
using System;
using System.Collections.Generic;
using System.Text;

namespace SabberStoneCoreAi.src.Agent.NEAT
{
    class NEATAgent: AbstractAgent
    {
		protected NEATGenome genome;

		public static readonly int NUM_INPUTS = 43;
		protected int numNodes = NUM_INPUTS+1;

		public NEATAgent()
		{
			genome = new NEATGenome(NUM_INPUTS);
		}

		public NEATAgent(NEATGenome g)
		{
			genome = g;
			int maxId = 33;
			foreach (ConnectionGene c in genome.Connections)
			{
				maxId = Math.Max(Math.Max(maxId, c.InNode), c.OutNode);
			}
			numNodes = maxId + 1;
		}

		public override void FinalizeAgent()
		{
			//throw new NotImplementedException();
		}

		public override void FinalizeGame()
		{
			//throw new NotImplementedException();
		}

		public override PlayerTask GetMove(POGame.POGame poGame)
		{
			List<PlayerTask> options = poGame.CurrentPlayer.Options();
			PlayerTask pick = options[0]; // Assuming there is at least one option...
			float pick_value = -10000f;

			foreach (PlayerTask t in options)
			{
				float val = CalcTotalOutput(poGame, t, numNodes);
				if (val > pick_value)
				{
					pick = t;
					pick_value = val;
					//Console.WriteLine(val);
				}
			}

			return pick;
		}

		public override void InitializeAgent()
		{
			//throw new NotImplementedException();
		}

		public override void InitializeGame()
		{
			//throw new NotImplementedException();
		}

		private float CalcTotalOutput(POGame.POGame poGame, PlayerTask t, int numNodes)
		{
			float[] inputs = GetInputs(poGame, t);
			float[] nodeActivations = new float[numNodes - NUM_INPUTS];

			// allow output to stabilize
			for (int i = 0; i < 3; i++)
			{
				for (int id = NUM_INPUTS; id < numNodes; id++) // foreach node
				{
					float nextActivation = 0;
					foreach (ConnectionGene g in genome.Connections)
					{
						if (g.OutNode == id)
						{
							if (g.InNode < NUM_INPUTS)
							{
								nextActivation += inputs[g.InNode] * g.Weight;
							}
							else
							{
								nextActivation += nodeActivations[g.InNode - NUM_INPUTS] * g.Weight;
							}
						}
					}
					// Using sigmoid since values get rather large and normalization doesn't play neat with NEAT
					nodeActivations[id - NUM_INPUTS] = Softsign(nextActivation);
				}
			}

			return nodeActivations[0]; // output node per definition
		}

		private float Softsign(float x)
		{
			return x / (1 + Math.Abs(x));
		}

		private float Sigmoid(float x)
		{
			return 1.0f / (1.0f + (1.0f / (float) Math.Exp(x)));
		}

		private float ReLU(float x)
		{
			return Math.Max(0, x);
		}

		// Bias included
		private float[] GetInputs(POGame.POGame game, PlayerTask action)
		{
			Controller player = game.CurrentPlayer;
			Controller opponent = game.CurrentOpponent;

			// Game related stuff
			// Values for own minions (sum and max)
			// Values for enemy minions (sum and max)
			float ownMinions = 0;
			float enemyMinions = 0;

			float numOwnTaunts = 0;
			float numEnemyTaunts = 0;

			float numOwnCanAttack = 0;
			float numEnemyCanAttack = 0;

			float ownSumHealth = 0;
			float ownMaxHealth = 0;
			float enemySumHealth = 0;
			float enemyMaxHealth = 0;

			float ownMaxHealthSum = 0;
			float ownMaxMaxHealth = 0;
			float enemyMaxHealthSum = 0;
			float enemyMaxMaxHealth = 0;

			float ownSumDmg = 0;
			float ownMaxDmg = 0;
			float enemySumDmg = 0;
			float enemyMaxDmg = 0;

			float ownSumDef = 0;
			float ownMaxDef = 0;
			float enemySumDef = 0;
			float enemyMaxDef = 0;


			foreach (Minion m in game.Minions)
			{
				if (m.IsDead)
				{
					continue;
				}

				if (m.Controller == opponent)
				{
					enemyMinions++;
					numEnemyTaunts += (m.HasTaunt) ? 1 : 0;
					numEnemyCanAttack += (m.CanAttack) ? 1 : 0;
					enemySumHealth += m.Health;
					enemyMaxHealth = Math.Max(enemyMaxHealth, m.Health);
					enemyMaxHealthSum += m.BaseHealth;
					enemyMaxMaxHealth = Math.Max(enemyMaxMaxHealth, m.BaseHealth);
					enemySumDmg += m.AttackDamage;
					enemyMaxDmg = Math.Max(enemyMaxDmg, m.AttackDamage);
					enemySumDef += m.Armor;
					enemyMaxDef = Math.Max(enemyMaxDef, m.Armor);
				}
				else
				{
					ownMinions++;
					numOwnTaunts += (m.HasTaunt) ? 1 : 0;
					numOwnCanAttack += (m.CanAttack) ? 1 : 0;
					ownSumHealth += m.Health;
					ownMaxHealth = Math.Max(ownMaxHealth, m.Health);
					ownMaxHealthSum += m.BaseHealth;
					ownMaxMaxHealth = Math.Max(ownMaxMaxHealth, m.BaseHealth);
					ownSumDmg += m.AttackDamage;
					ownMaxDmg = Math.Max(ownMaxDmg, m.AttackDamage);
					ownSumDef += m.Armor;
					ownMaxDef = Math.Max(ownMaxDef, m.Armor);
				}
			}

			// Hero stuff
			float ownHeroCanAttack = 0;
			float enemyHeroCanAttack = 0;

			float ownHeroHealth = 0;
			float enemyHeroHealth = 0;

			float ownHeroBaseHealth = 0;
			float enemyHeroBaseHealth = 0;

			float ownHeroDmg = 0;
			float enemyHeroDmg = 0;

			float ownHeroDef = 0;
			float enemyHeroDef = 0;

			foreach (Hero h in game.Heroes)
			{
				if (h.IsDead)
					continue; // Kinda useless?

				if (h.Controller == opponent)
				{
					enemyHeroCanAttack += (h.CanAttack) ? 1 : 0;
					enemyHeroHealth += h.Health;
					enemyHeroBaseHealth += h.BaseHealth;
					enemyHeroDmg += h.AttackDamage;
					enemyHeroDef += h.Armor;
				}
				else
				{
					ownHeroCanAttack += (h.CanAttack) ? 1 : 0;
					ownHeroHealth += h.Health;
					ownHeroBaseHealth += h.BaseHealth;
					ownHeroDmg += h.AttackDamage;
					ownHeroDef += h.Armor;
				}
			}

			// task related stuff
			// Task type
			float[] type = oneHot((int) action.PlayerTaskType, 7);
			// 1 info per type
			float type_info = 0;
			float has_target = action.HasTarget ? 1 : -1;
			float has_source = action.HasSource ? 1 : -1;
			//float target_class = action.HasTarget ? (float)action.Target.Card.Class : -1;
			//float source_class = action.HasSource ? (float)action.Source.Card.Class : -1;
			if (action.Source != null)
			{
				switch (action.PlayerTaskType)
				{
					case PlayerTaskType.HERO_ATTACK:
						{
							Hero h = (Hero)action.Source;
							if (h.CanAttack)
							{
								type_info = h.AttackDamage;
							}
							break;
						}
					case PlayerTaskType.MINION_ATTACK:
						{
							Minion m = (Minion)action.Source;
							if (m.CanAttack)
							{
								type_info = m.AttackDamage;
							}
							break;
						}
					case PlayerTaskType.PLAY_CARD:
						{
							if (null != action.Source)
							{
								type_info = -1f;
								break;
							}
							Entity m = (Entity)action.Source;
							type_info = (float)m.Card.Class;
							break;
						}
				}
			}
			return new float[]{
				ownMinions,
				enemyMinions,
				numOwnTaunts,
				numEnemyTaunts,
				numOwnCanAttack,
				numEnemyCanAttack,
				ownSumHealth,
				ownMaxHealth,
				ownMaxHealthSum,
				ownMaxMaxHealth,
				enemySumHealth,
				enemyMaxHealth,
				enemyMaxHealthSum,
				enemyMaxMaxHealth,
				ownSumDmg,
				ownMaxDmg,
				enemySumDmg,
				enemyMaxDmg,
				ownSumDef,
				ownMaxDef,
				enemySumDef,
				enemyMaxDef,
				ownHeroCanAttack,
				enemyHeroCanAttack,
				ownHeroHealth,
				ownHeroBaseHealth,
				enemyHeroHealth,
				enemyHeroBaseHealth,
				ownHeroDmg,
				enemyHeroDmg,
				ownHeroDef,
				enemyHeroDef,
				type[0],
				type[1],
				type[2],
				type[3],
				type[4],
				type[5],
				type[6],
				type_info,
				has_target,
				has_source,
				//target_class,
				//source_class,
				1
			};
		}

		private float[] oneHot(int idx, int num)
		{
			float[] tmp = new float[num];
			for(int i = 0; i < num; i++)
			{
				tmp[i] = (i == idx) ? 1 : 0;
			}
			return tmp;
		}
	}
}
