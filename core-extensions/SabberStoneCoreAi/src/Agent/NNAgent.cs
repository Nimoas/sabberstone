﻿using SabberStoneCore.Tasks;
using SabberStoneCoreAi.Agent;
using SabberStoneCoreAi.POGame;
using System;
using System.Collections.Generic;
using System.Text;

namespace SabberStoneCoreAi.src.Agent
{
	class NNAgent : AbstractAgent
	{
		protected NNPhenotype pheno;
		protected NNGenotype geno;

		public NNAgent()
		{

			geno = new NNGenotype();
			pheno = new NNPhenotype(geno);
		}

		public NNAgent(NNPhenotype ph)
		{
			geno = ph.Genotype;
			pheno = ph;
		}

		protected NNAgent(string path)
		{
			geno = new NNGenotype(path);
			pheno = new NNPhenotype(geno);
		}

		public override void FinalizeAgent()
		{
			//throw new NotImplementedException();
		}

		public override void FinalizeGame()
		{
			//throw new NotImplementedException();
		}

		public override PlayerTask GetMove(POGame.POGame poGame)
		{
			List<PlayerTask> options = poGame.CurrentPlayer.Options();
			PlayerTask pick = options[0]; // Assuming there is at least one option...
			float pick_value = 0;

			foreach (PlayerTask t in options)
			{
				float val = pheno.CalcTotalOutput(poGame, t);
				if (val > pick_value)
				{
					pick = t;
					pick_value = val;
				}
			}

			return pick;
		}

		public override void InitializeAgent()
		{
			//throw new NotImplementedException();
		}

		public override void InitializeGame()
		{
			//throw new NotImplementedException();
		}

		
	}
}
