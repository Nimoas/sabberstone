﻿using SabberStoneCore.Model;
using SabberStoneCore.Model.Entities;
using SabberStoneCore.Tasks;
using System;
using System.Collections.Generic;

namespace SabberStoneCoreAi.src.Agent
{
	class NNPhenotype
	{
		// layer, neuron, input
		float[][][] weights;
		float fintess = 0;
		NNGenotype genotype;

		public float Fintess { get => fintess; set => fintess = value; }
		internal NNGenotype Genotype { get => genotype; }

		public NNPhenotype(NNGenotype genotype)
		{
			this.genotype = genotype;
			weights = new float[3][][];
			// Layers (incl Input) are 28x10x5x1, all +1 for bias
			// first set of weights
			weights[0] = new float[10][];
			for (int i = 0; i < 10; i++)
			{
				weights[0][i] = new float[29];
				for (int j = 0; j < 29; j++)
				{
					weights[0][i][j] = genotype.Data[29 * i + j];
				}
			}

			// second layer, offset is 290
			weights[1] = new float[5][];
			for (int i = 0; i < 5; i++)
			{
				weights[1][i] = new float[11];
				for (int j = 0; j < 11; j++)
				{
					weights[1][i][j] = genotype.Data[290 + i * 11 + j];
				}
			}

			// output neuron, offset is 290+55=345
			weights[2] = new float[1][];
			weights[2][0] = new float[6];
			for (int j = 0; j < 6; j++)
			{
				weights[2][0][j] = genotype.Data[345 + j];
			}
		}

		public float CalcTotalOutput(POGame.POGame game, PlayerTask task)
		{
			float[] inputs = GetInputs(game, task);

			// set input to the output of the latest layer
			for (int i = 0; i < weights.Length; i++)
			{
				inputs = CalcLayerOutput(weights[i], inputs);
			}

			return inputs[0]; // only one output left

		}

		private float[] CalcLayerOutput(float[][] weights, float[] inputs)
		{
			float[] acc = new float[weights.Length];

			for (int i = 0; i < weights.Length; i++)
			{
				acc[i] = CalcNeuronOutput(weights[i], inputs);
			}

			return acc;
		}

		/**
		 * weights needs to be 1 longer than inputs (bias)
		 */
		private float CalcNeuronOutput(float[] weights, float[] inputs)
		{
			float acc = 0;

			// bias
			acc += weights[0];

			for (int i = 0; i < inputs.Length; i++)
			{
				acc += inputs[i] * weights[i + 1];
			}

			return ReLU(acc);
		}

		private float ReLU(float x)
		{
			return Math.Max(0, x);
		}

		private float[] GetInputs(POGame.POGame game, PlayerTask action)
		{
			Controller player = game.CurrentPlayer;
			Controller opponent = game.CurrentOpponent;

			// Game related stuff
			// Values for own minions (sum and max)
			// Values for enemy minions (sum and max)
			float ownMinions = 0;
			float enemyMinions = 0;

			float numOwnTaunts = 0;
			float numEnemyTaunts = 0;

			float numOwnCanAttack = 0;
			float numEnemyCanAttack = 0;

			float ownSumHealth = 0;
			float ownMaxHealth = 0;
			float enemySumHealth = 0;
			float enemyMaxHealth = 0;

			float ownSumDmg = 0;
			float ownMaxDmg = 0;
			float enemySumDmg = 0;
			float enemyMaxDmg = 0;

			float ownSumDef = 0;
			float ownMaxDef = 0;
			float enemySumDef = 0;
			float enemyMaxDef = 0;


			foreach (Minion m in game.Minions)
			{
				if (m.IsDead)
				{
					continue;
				}

				if (m.Controller == opponent)
				{
					enemyMinions++;
					numEnemyTaunts += (m.HasTaunt) ? 1 : 0;
					numEnemyCanAttack += (m.CanAttack) ? 1 : 0;
					enemySumHealth += m.Health;
					enemyMaxHealth = Math.Max(enemyMaxHealth, m.Health);
					enemySumDmg += m.AttackDamage;
					enemyMaxDmg = Math.Max(enemyMaxDmg, m.AttackDamage);
					enemySumDef += m.Armor;
					enemyMaxDef = Math.Max(enemyMaxDef, m.Armor);
				}
				else
				{
					ownMinions++;
					numOwnTaunts += (m.HasTaunt) ? 1 : 0;
					numOwnCanAttack += (m.CanAttack) ? 1 : 0;
					ownSumHealth += m.Health;
					ownMaxHealth = Math.Max(ownMaxHealth, m.Health);
					ownSumDmg += m.AttackDamage;
					ownMaxDmg = Math.Max(ownMaxDmg, m.AttackDamage);
					ownSumDef += m.Armor;
					ownMaxDef = Math.Max(ownMaxDef, m.Armor);
				}
			}

			// Hero stuff
			float ownHeroCanAttack = 0;
			float enemyHeroCanAttack = 0;

			float ownHeroHealth = 0;
			float enemyHeroHealth = 0;

			float ownHeroDmg = 0;
			float enemyHeroDmg = 0;

			float ownHeroDef = 0;
			float enemyHeroDef = 0;

			foreach (Hero h in game.Heroes)
			{
				if (h.IsDead)
					continue; // Kinda useless?

				if (h.Controller == opponent)
				{
					enemyHeroCanAttack += (h.CanAttack) ? 1 : 0;
					enemyHeroHealth += h.Health;
					enemyHeroDmg += h.AttackDamage;
					enemyHeroDef += h.Armor;
				}
				else
				{
					ownHeroCanAttack += (h.CanAttack) ? 1 : 0;
					ownHeroHealth += h.Health;
					ownHeroDmg += h.AttackDamage;
					ownHeroDef += h.Armor;
				}
			}

			// task related stuff
			// Task type
			float type = (float) action.PlayerTaskType;
			// 1 info per type
			float type_info = 0;
			if (action.Source != null)
			{
				switch (action.PlayerTaskType)
				{
					case PlayerTaskType.HERO_ATTACK:
						{
							Hero h = (Hero)action.Source;
							if (h.CanAttack)
							{
								type_info = h.AttackDamage;
							}
							break;
						}
					case PlayerTaskType.MINION_ATTACK:
						{
							Minion m = (Minion)action.Source;
							if (m.CanAttack)
							{
								type_info = m.AttackDamage;
							}
							break;
						}
						case PlayerTaskType.PLAY_CARD:
							{
								if (null != action.Source)
								{
									type_info = -1f;
									break;
								}
								Entity m = (Entity) action.Source;
								type_info = (float) m.Card.Class;
								break;
							}
				}
			}
			return new float[]{
				ownMinions,
				enemyMinions,
				numOwnTaunts,
				numEnemyTaunts,
				numOwnCanAttack,
				numEnemyCanAttack,
				ownSumHealth,
				ownMaxHealth,
				enemySumHealth,
				enemyMaxHealth,
				ownSumDmg,
				ownMaxDmg,
				enemySumDmg,
				enemyMaxDmg,
				ownSumDef,
				ownMaxDef,
				enemySumDef,
				enemyMaxDef,
				ownHeroCanAttack,
				enemyHeroCanAttack,
				ownHeroHealth,
				enemyHeroHealth,
				ownHeroDmg,
				enemyHeroDmg,
				ownHeroDef,
				enemyHeroDef,
				type,
				type_info
			};
		}
	}
}
