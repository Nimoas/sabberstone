﻿using System;
using SabberStoneCore.Config;
using SabberStoneCore.Enums;
using SabberStoneCoreAi.POGame;
using SabberStoneCoreAi.Agent.ExampleAgents;
using SabberStoneCoreAi.Agent;
using SabberStoneCoreAi.src.Agent;
using SabberStoneCoreAi.src;
using SabberStoneCoreAi.Meta;
using SabberStoneCoreAi.src.Agent.NEAT;

namespace SabberStoneCoreAi
{
	internal class Program
	{

		private static void Main(string[] args)
		{
			/*Console.WriteLine("Start Training");

			new NEATTraining().RunTraining();

			Console.ReadLine();
			*/
			Console.WriteLine("Setup gameConfig");

			//todo: rename to Main
			GameConfig gameConfig = new GameConfig
			{
				Player1HeroClass = CardClass.WARRIOR,
				Player2HeroClass = CardClass.WARRIOR,
				Player1Deck = Decks.AggroPirateWarrior,
				Player2Deck = Decks.AggroPirateWarrior,
				Logging = false
			};

			Console.WriteLine("Setup POGameHandler");
			AbstractAgent player1 = new NEATFixedAgent();
			AbstractAgent player2 = new RandomAgentLateEnd();
			var gameHandler = new POGameHandler(gameConfig, player1, player2, debug:false);

			Console.WriteLine("PlayGame");
			gameHandler.PlayGames(10000);
			GameStats gameStats = gameHandler.getGameStats();

			gameStats.printResults();


			Console.WriteLine("Test successful");
			Console.ReadLine();
		}
	}
}
