﻿using SabberStoneCore.Config;
using SabberStoneCore.Enums;
using SabberStoneCore.Model;
using SabberStoneCoreAi.Agent;
using SabberStoneCoreAi.Agent.ExampleAgents;
using SabberStoneCoreAi.Meta;
using SabberStoneCoreAi.POGame;
using SabberStoneCoreAi.src.Agent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SabberStoneCoreAi.src
{
	// Do EA here
    class Training
    {
		const int MAX_EPOCHS = 1000;
		const int POP_SIZE = 75;
		const int EVAL_RUNS = 50; 
		const int NUM_OFFSPRING = 80; // must be even
		const int TOURNAMENT_SIZE = 2;
		const string BASE_PATH = "C:\\Training\\Random_Mirror_Pirate\\";

		Random rng = new Random();


		public void RunTraining()
		{
			int epoch = 0;
			List<NNPhenotype> pop = new List<NNPhenotype>(POP_SIZE + NUM_OFFSPRING);

			if (Directory.Exists(BASE_PATH))
			{
				string[] subdirs = Directory.GetDirectories(BASE_PATH);
				if (subdirs.Length > 0)
				{
					int max = -1;
					foreach (string s in subdirs)
					{
						int e = Int32.Parse(Path.GetFileName(s));
						max = Math.Max(max, e);
					}
					if (max >= 0)
					{
						epoch = max + 1;
						string path = BASE_PATH + max + "\\";
						for (int i = 0; i < POP_SIZE; i++)
						{
							NNGenotype gene = new NNGenotype(path + i);
							NNPhenotype ph = new NNPhenotype(gene);
							Evaluate(ph);
							pop.Add(ph);
						}
					} else
					{
						// Random init of population
						for (int i = 0; i < POP_SIZE; i++)
						{
							NNGenotype gene = new NNGenotype();
							NNPhenotype ph = new NNPhenotype(gene);
							Evaluate(ph);
							pop.Add(ph);
						}
					}
				} else
				{
					// Random init of population
					for (int i = 0; i < POP_SIZE; i++)
					{
						NNGenotype gene = new NNGenotype();
						NNPhenotype ph = new NNPhenotype(gene);
						Evaluate(ph);
						pop.Add(ph);
					}
				}
			}
			else
			{
				// Random init of population
				for (int i = 0; i < POP_SIZE; i++)
				{
					NNGenotype gene = new NNGenotype();
					NNPhenotype ph = new NNPhenotype(gene);
					Evaluate(ph);
					pop.Add(ph);
				}
			}
			NNPhenotype best = pop[0];
			float bestFitness = 0;

			while(epoch < MAX_EPOCHS && bestFitness < 1.0f)
			{
				Console.WriteLine("Epoch {0}", epoch);

				//Mating
				List<NNPhenotype> offspring = new List<NNPhenotype>(NUM_OFFSPRING);
				for (int i = 0; i < NUM_OFFSPRING; i += 2)
				{
					NNPhenotype parent1 = TournamentSelection(pop);
					NNPhenotype parent2 = TournamentSelection(pop);

					NNGenotype new_gene = NNGenotype.Crossover(parent1.Genotype, parent2.Genotype, rng);
					new_gene.Mutate(rng);
					NNPhenotype off = new NNPhenotype(new_gene);
					Evaluate(off);
					offspring.Add(off);
					//Console.WriteLine("\tCreated Offspring.");

					NNGenotype new_gene2 = NNGenotype.Crossover(parent2.Genotype, parent1.Genotype, rng);
					new_gene.Mutate(rng);
					NNPhenotype off2 = new NNPhenotype(new_gene2);
					Evaluate(off2);
					offspring.Add(off2);
					//Console.WriteLine("\tCreated Offspring.");
				}
				pop.AddRange(offspring);

				Console.WriteLine("\tFinished Mating.");

				// Find best and keep it
				foreach (NNPhenotype ph in pop)
				{
					if (ph.Fintess > bestFitness)
					{
						best = ph;
						bestFitness = ph.Fintess;
					}
				}

				// Print statistics
				Console.WriteLine("\tBest fitness is {0}", bestFitness);
				float avrg = 0;
				foreach (NNPhenotype ph in pop)
				{
					avrg += ph.Fintess;
				}
				avrg /= (float) pop.Count;
				Console.WriteLine("\tAvrg. fitness is {0}", avrg);

				// new population
				List<NNPhenotype> nextGen = new List<NNPhenotype>(POP_SIZE + NUM_OFFSPRING);
				// take best
				nextGen.Add(best);

				for (int i = 1; i < POP_SIZE; i++)
				{
					nextGen.Add(TournamentSelection(pop));
				}
				pop = nextGen;

				// Save stuff to disk... VM crashed quite regularly
				string path = BASE_PATH + epoch + "\\";
				Directory.CreateDirectory(path);
				best.Genotype.SaveTo(path + "best");
				for (int i = 0; i < POP_SIZE; i++)
				{
					string filename = path + i;
					pop[i].Genotype.SaveTo(filename);
				}

				epoch++;
			}


		}

		private NNPhenotype TournamentSelection(List<NNPhenotype> pop)
		{
			NNPhenotype best = pop[0];
			float fitness = 0;

			for (int i = 0; i < TOURNAMENT_SIZE; i++)
			{
				int idx = rng.Next(pop.Count);
				NNPhenotype ph = pop[idx];
				if (ph.Fintess >= fitness)
				{
					best = ph;
					fitness = ph.Fintess;
				}
			}

			return best;
		}

		private void Evaluate(NNPhenotype ph)
		{
			float avrg = 0;

			avrg += SubEvaluate(ph, Decks.AggroPirateWarrior, Decks.AggroPirateWarrior, CardClass.WARRIOR, CardClass.WARRIOR);
			//avrg += SubEvaluate(ph, Decks.AggroPirateWarrior, Decks.RenoKazakusMage, CardClass.WARRIOR, CardClass.MAGE);
			//avrg += SubEvaluate(ph, Decks.AggroPirateWarrior, Decks.MidrangeJadeShaman, CardClass.WARRIOR, CardClass.SHAMAN);

			/*avrg += SubEvaluate(ph, Decks.RenoKazakusMage, Decks.AggroPirateWarrior, CardClass.MAGE, CardClass.WARRIOR);
			avrg += SubEvaluate(ph, Decks.RenoKazakusMage, Decks.RenoKazakusMage, CardClass.MAGE, CardClass.MAGE);
			avrg += SubEvaluate(ph, Decks.RenoKazakusMage, Decks.MidrangeJadeShaman, CardClass.MAGE, CardClass.SHAMAN);
			
			avrg += SubEvaluate(ph, Decks.MidrangeJadeShaman, Decks.AggroPirateWarrior, CardClass.SHAMAN, CardClass.WARRIOR);
			avrg += SubEvaluate(ph, Decks.MidrangeJadeShaman, Decks.RenoKazakusMage, CardClass.SHAMAN, CardClass.MAGE);
			avrg += SubEvaluate(ph, Decks.MidrangeJadeShaman, Decks.MidrangeJadeShaman, CardClass.SHAMAN, CardClass.SHAMAN);
			
			avrg /= 6.0f;*/
			ph.Fintess = avrg;
		}

		private float SubEvaluate(NNPhenotype ph, List<Card> d1, List<Card> d2, CardClass c1, CardClass c2)
		{
			GameConfig gameConfig = new GameConfig
			{
				Player1Deck = d1,
				Player2Deck = d2,
				Player1HeroClass = c1,
				Player2HeroClass = c2,
				Logging = false
			};

			AbstractAgent player1 = new NNAgent(ph);
			AbstractAgent player2 = new RandomAgentLateEnd();
			var gameHandler = new POGameHandler(gameConfig, player1, player2, debug: false);
			
			//gameHandler.PlayGame();
			gameHandler.PlayGames(EVAL_RUNS);
			GameStats gameStats = gameHandler.getGameStats();

			//gameStats.printResults();
			return (float)gameStats.PlayerA_Wins / (float)gameStats.GamesPlayed;

		}
	}
}
